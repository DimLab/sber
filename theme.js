import { StyleSheet } from 'react-native';

const colors = {
	text: '#555555',
	border: '#c2c2c2',
	primary: '#2E7BF6'
};


export default {
	colors,
	styles: StyleSheet.create ({
		formElem: {
			color: colors.text,
			textAlign: 'center',
			fontSize: 20,
			height: 60,
			paddingHorizontal: 10,
			borderRadius: 6,
			margin: 10,
			maxWidth: 300,
			width: '100%',
			borderWidth: 1,
			borderColor: colors.border
		},
		title: {
			fontSize: 20,
			color: colors.text,
			textAlign: 'center'
		},
		disabled: {
			opacity: 0.3
		}
	})
};
