import React from 'react';
import { Slider, StyleSheet } from 'react-native';
import theme from 'theme';
import PropTypes from 'prop-types';


export default React.memo ( FormSlider );

function FormSlider ({ style, ...rest })
{
	return (
		<Slider
			style={[ theme.styles.formElem, styles.slider, style, { color: theme.colors.primary } ]}
			minimumValue={ 0 }
			maximumValue={ 10 }
			step={ 1 }
			thumbTintColor={ theme.colors.primary }
            minimumTrackTintColor={ theme.colors.primary }
			{ ...rest }
		/>
	);
}


const styles = StyleSheet.create ({
	slider: {
		borderWidth: 0
	}
});

Slider.propTypes = {
	style: PropTypes.oneOfType ([
		PropTypes.number, // don't use inlinr styles
		PropTypes.array
	]),
};
