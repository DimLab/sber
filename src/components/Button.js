import React from 'react';
import {
	Text,
	TouchableOpacity,
	StyleSheet
} from 'react-native';
import theme from 'theme';
import PropTypes from 'prop-types';


export default React.memo ( Button );

function Button ({ style, title, disabled, ...rest })
{
	return (
		<TouchableOpacity
			style={[ styles.container, theme.styles.formElem, disabled && theme.styles.disabled, style ]}
			{...{ disabled }}
			{ ...rest }
		>
			<Text style={ theme.styles.title }>{ title }</Text>
		</TouchableOpacity>
	);
}


const styles = StyleSheet.create ({
	container: {
		alignItems: 'center',
		justifyContent: 'center'
	}
});

Button.propTypes = {
	style: PropTypes.oneOfType ([
		PropTypes.number, // don't use inlinr styles
		PropTypes.array
	]),
	title: PropTypes.string.isRequired,
	disabled: PropTypes.bool,
	onPress: PropTypes.func.isRequired
};
