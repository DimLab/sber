import React from 'react';
import { TextInput } from 'react-native';
import theme from 'theme';
import PropTypes from 'prop-types';


export default React.memo ( TextField );

function TextField ({ style, ...rest })
{
	return (
		<TextInput
			style={[ theme.styles.formElem, style ]}
			placeholder="Введите число"
			keyboardType="numeric"
			underlineColorAndroid="transparent"
			{ ...rest }
		/>
	);
}


TextField.propTypes = {
	style: PropTypes.oneOfType ([
		PropTypes.number, // don't use inlinr styles
		PropTypes.array
	])
};
