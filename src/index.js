import React, { useState, useCallback } from 'react';
import {
	SafeAreaView,
	ScrollView,
	Text,
	StyleSheet
} from 'react-native';
import { TextField, FormSlider, Button } from 'components';
import theme from 'theme';


export default function App ()
{
	const [ value1, setValue1 ] = useState ( '' ),
		[ value2, setValue2 ] = useState ( 5 ),
		[ res, setRes ] = useState ( '' ),
		calc = useCallback (
			() => setRes ( String ( parseInt ( value1, 10 ) * value2 ) ),
			[ value1, value2 ]
		),
		disabled = value1 === '';

	return (
		<SafeAreaView style={ styles.container }>
			<ScrollView
				style={ styles.scrollView }
				contentContainerStyle={ styles.content }
				keyboardShouldPersistTaps="handled"
				showsVerticalScrollIndicator={ false }
			>
				<Text style={[ theme.styles.title, styles.title ]}>Мини калькулятор</Text>
				<TextField
					placeholder="Введите число"
					onChangeText={ setValue1 }
					value={ value1 }
				/>
				<FormSlider
					onValueChange={ setValue2 }
					value={ value2 }
				/>
				<Button
					title="Рассчитать"
					onPress={ calc }
					{...{ disabled }}
				/>
				<TextField
					placeholder="Результат"
					value={ res }
					editable={ false }
				/>
			</ScrollView>
		</SafeAreaView>
	);
}


const styles = StyleSheet.create ({
	container: {
		flex: 1,
		alignItems: 'stretch'
	},
	scrollView: {
		flex: 1
	},
	content: {
		alignItems: 'center',
		paddingBottom: 20
	},
	title: {
		margin: 20
	}
});
